/**
 * Project: PRL 3 - Mesh Multiplication
 *  Author: Daniel Haris
 *   Login: xharis00
*/

#include "mm.h"

/**
 * It is example code from: http://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/
 * @param start from
 * @param end to
 * @return difference
 */
timespec timeDiff(timespec start, timespec end) {
    timespec temp;
    if ((end.tv_nsec - start.tv_nsec) < 0) {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}

/**
 * @brief Function converts time value to nanoseconds
 * @param time time value
 * @return time value in nanoseconds
 */
double timeToNanoSec(timespec time) { return (double)time.tv_sec * 1000000000.0 + (double)time.tv_nsec; }

/**
 * @brief Function implements the MPI waiting for an INT value, also checks for MPI errors
 * @param value output value
 * @param cpuId CPU ID
 * @param tag MPI message tag
 */
void mpiRecvInt(int* value, int cpuId, int tag) {
    MPI_Status status;
    MPI_Recv(value, 1, MPI_INT, cpuId, tag, MPI_COMM_WORLD, &status);
    if (status.MPI_ERROR) {
        cerr << "MPI_Recv err: " << status.MPI_ERROR << endl;
        MPI_Abort(MPI_COMM_WORLD, ERE_RECV);
    }
}

/**
 * @brief Function loads numbers from file to a vector of integers
 * @param fs file stream
 * @return vector of integer values
 */
vector<int> readNumbers(std::fstream& fs) {
    vector<int> numbers;
    for (int number; fs >> number;) numbers.push_back(number);
    return numbers;
}

/**
 * @brief Function loads the first matrix, the first line should contain the number of rows of the matrix
 * @param filePath path to a file
 * @return matrix
 */
Matrix readMatrix1(string filePath) {
    std::fstream fs(filePath, std::ios_base::in);
    if (!fs.is_open()) {
        cerr << "Could not open file: " << filePath << endl;
        MPI_Abort(MPI_COMM_WORLD, ERR_FILE);
    }
    int rows, cols;
    fs >> rows;
    vector<int> numbers = readNumbers(fs);
    fs.close();
    cols = (int) (numbers.size() / rows);
    return Matrix((size_t) rows, (size_t) cols, numbers);
}

/**
 * @brief Function loads the second matrix, the first line should contain the number of columns of the matrix
 * @param filePath path to a file
 * @return matrix
 */
Matrix readMatrix2(string filePath) {
    std::fstream fs(filePath, std::ios_base::in);
    if (!fs.is_open()) {
        cerr << "Could not open file: " << filePath << endl;
        MPI_Abort(MPI_COMM_WORLD, ERR_FILE);
    }
    int rows, cols;
    fs >> cols;
    vector<int> numbers = readNumbers(fs);
    fs.close();
    rows = (int)numbers.size() / cols;
    return Matrix((size_t) rows, (size_t) cols, numbers);
}

/**
 * @brief Function checks if the 2 matrices have correct dimensions
 * @param mat1 first matrix
 * @param mat2 second matrix
 */
void checkInputDimensions(Matrix &mat1, Matrix &mat2) {
    if (mat1.getColCount() != mat2.getRowCount()) {
        cerr << "Mat1 rows and mat2 cols must be equal!\nCurrent values:\n"
             << "mat1.col = " << mat1.getColCount()
             << "mat2.row = " << mat2.getRowCount() << endl;
        MPI_Abort(MPI_COMM_WORLD, ERR_MATRIX);
    }
}

/**
 * @brief Function checks if there is correct number of CPUs for given matrices
 * @param numOfCPUs number of CPUs
 * @param rows count of rows of the first matrix
 * @param cols count of columns of the second matrix
 */
void checkProcessCount(int numOfCPUs, int rows, int cols) {
    if (numOfCPUs != (rows * cols)) {
        cerr << "Invalid CPU count for input matrices!"
             << "\nExpected:" << rows * cols
             << "\nActual: " << numOfCPUs << endl;
        MPI_Abort(MPI_COMM_WORLD, ERR_PROCESS_COUNT);
    }
}

/**
 * @brief Hlavni zpracovani vstupnich dat. Postupne se prochazi vsechny hodnoty
 *        vstupnich matic a ty odesilaji ostatnim procesum.
 * @param cpuId identifikacni struktura procesu
 * @param mat1 reference na prvni matici
 * @param mat2 reference na druhou matici
 * @return vysledna hodnota hlavni procesu (typicky procesu 0,0)
 */
int processData(CPU &cpuId, Matrix &mat1, Matrix &mat2) {
    size_t setCount = mat1.getColCount();
    int cpuValue = 0;
    for (size_t set = 0; set < setCount; set++) {
        // Hodnoty pro hlavni procesor hned vynasobime a posleme dal
        int rowValue = mat1.get(0, setCount - set - 1);
        if (!cpuId.lastCol) MPI_Send(&rowValue, 1, MPI_INT, cpuId.getRightCPUId(), TAG_FROM_LEFT, MPI_COMM_WORLD);
        int colValue = mat2.get(setCount - set - 1, 0);
        if (!cpuId.lastRow) MPI_Send(&colValue, 1, MPI_INT, cpuId.getDownCPUId(), TAG_FROM_UP, MPI_COMM_WORLD);
        cpuValue += rowValue * colValue;
        // Odeslani hodnot dalsim procesum, ktere se nachazeji na okrajich matice
        for (size_t rowIndex = 1; rowIndex < mat1.getRowCount(); rowIndex++) {
            int id = cpuId.getIdFirstInRow((int) rowIndex);
            int value = mat1.get(rowIndex, setCount - set - 1);
            MPI_Send(&value, 1, MPI_INT, id, TAG_FROM_LEFT, MPI_COMM_WORLD);
        }
        for (size_t colIndex = 1; colIndex < mat2.getColCount(); colIndex++) {
            int id = cpuId.getIdFirstInCol((int) colIndex);
            int value = mat2.get(setCount - set - 1, colIndex);
            MPI_Send(&value, 1, MPI_INT, id, TAG_FROM_UP, MPI_COMM_WORLD);
        }
    }
    return cpuValue;
}

/**
 * @brief Prijmuti konecnych hodnot od vsech procesu. Rozmery matic slouzci pro
 *        sestaveni vysledne matice.
 * @param cpuId identifikacni struktu procesu
 * @param rows pocet radku prvni matice
 * @param cols pocet sloupcu druhe matice
 * @return vysledna matice (nasobek vstupnich matic)
 */
Matrix recvFinalData(CPU &cpuId, size_t rows, size_t cols) {
    Matrix result(rows, cols);
    for (int i = 1; i < cpuId.numOfCPUs; i++) {
        int value;
        mpiRecvInt(&value, i, TAG_OUT);
        result.set(value, CPU::idToIndex2d(i, cpuId.numOfMatrixCols));
    }
    return result;
}

int main(int argc, char *argv[])
{
    int numOfCPUs, myCPUId;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numOfCPUs); // pocet bezicich procesu
    MPI_Comm_rank(MPI_COMM_WORLD, &myCPUId); // id sveho procesu
    // the main CPU
    if (myCPUId == MAIN_CPU_ID) {
        int CPUVal, rows, cols, setCount;
        Matrix mat1, mat2;
        // load matrices
        mat1 = readMatrix1(FILE_PATH_MAT1);
        mat2 = readMatrix2(FILE_PATH_MAT2);
        // get dimensions
        rows = (int) mat1.getRowCount();
        cols = (int) mat2.getColCount();
        setCount = (int) mat1.getColCount();
        // input matrices and CPU count check
        checkInputDimensions(mat1, mat2);
        checkProcessCount(numOfCPUs, rows, cols);
        // create cpu class instance with provided information
        CPU cpu(myCPUId, numOfCPUs, rows, cols);
        // notify other CPUs about dimensions
        for (int i = 1; i < numOfCPUs; i++) {
            MPI_Send(&rows, 1, MPI_INT, i, TAG_ROWS_COUNT, MPI_COMM_WORLD);
            MPI_Send(&cols, 1, MPI_INT, i, TAG_COLS_COUNT, MPI_COMM_WORLD);
            MPI_Send(&setCount, 1, MPI_INT, i, TAG_SET_COUNT, MPI_COMM_WORLD);
        }
#ifdef STATISTIC_MEASURING
        timespec time1, time2, diff;
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
#endif
        // matrix pre-processing and compute
        CPUVal = processData(cpu, mat1, mat2);
        // wait for the multiplication result
        Matrix result = recvFinalData(cpu, mat1.getRowCount(), mat2.getColCount());
        // set main CPU value
        result.set(CPUVal, 0, 0);
#ifdef STATISTIC_MEASURING
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time2);
        diff = timeDiff(time1, time2);
        cout << diff.tv_nsec << endl;
#else
        result.print();
#endif
    // every other CPUs
    } else {
        // get information about input matrices
        int rowCount, colCount, setCount;
        mpiRecvInt(&rowCount, MAIN_CPU_ID, TAG_ROWS_COUNT);
        mpiRecvInt(&colCount, MAIN_CPU_ID, TAG_COLS_COUNT);
        mpiRecvInt(&setCount, MAIN_CPU_ID, TAG_SET_COUNT);
        // create a CPU class instance with provided information
        CPU cpu(myCPUId, numOfCPUs, rowCount, colCount);
        int val = 0;
        int a, b;
        // loop for receiving data and processing them
        for (int i = 0; i < setCount; i++) {
            // receive from left
            mpiRecvInt(&a, cpu.getLeftCPUId(), TAG_FROM_LEFT);
            // receive from up
            mpiRecvInt(&b, cpu.getUpCPUId(), TAG_FROM_UP);
            // multiplication
            val += a * b;
            // if not last column CPU then send value to right neighbor CPU
            if (!cpu.lastCol) MPI_Send(&a, 1, MPI_INT, cpu.getRightCPUId(), TAG_FROM_LEFT, MPI_COMM_WORLD);
            // if not last row CPU then send value to bottom CPU
            if (!cpu.lastRow) MPI_Send(&b, 1, MPI_INT, cpu.getDownCPUId(), TAG_FROM_UP, MPI_COMM_WORLD);
        }
        // sending the result to the main CPU
        MPI_Send(&val, 1, MPI_INT, MAIN_CPU_ID, TAG_OUT, MPI_COMM_WORLD);
    }
    MPI_Finalize();
    return 0;
}
