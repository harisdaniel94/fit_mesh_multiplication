/**
 * Project: PRL 3 - Mesh Multiplication
 *  Author: Daniel Haris
 *   Login: xharis00
*/

#include <mpi.h>
#include <algorithm>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

using namespace std;

/* Paths to files with the input matrices */
const string FILE_PATH_MAT1 = "mat1";
const string FILE_PATH_MAT2 = "mat2";
// uncomment to activate statistics measurements
//#define STATISTIC_MEASURING
// Error codes
#define ERR_FILE          1
#define ERR_MATRIX        2
#define ERR_PROCESS_COUNT 3
#define ERE_RECV          4
// tags for MPI communication
#define TAG_FROM_UP    1
#define TAG_FROM_LEFT  2
#define TAG_OUT        3
#define TAG_ROWS_COUNT 4
#define TAG_COLS_COUNT 5
#define TAG_SET_COUNT  6
// main CPU ID
#define MAIN_CPU_ID 0

/**
 * Structure for indexing items in 2D array
 */
struct Index2D {
    size_t row, col;
    Index2D(size_t row, size_t col) : row(row), col(col) {}
};

/**
 * Class represents a 2D array of integers with some defined methods
 */
class Matrix {
private:
    size_t rows, cols;
    vector<vector<int>> data;
public:
    /**
     * @brief Constructor
     */
    Matrix() : rows(0), cols(0) {}

    /**
     * @brief Constructor
     * @param numOfMatrixRows count of rows in matrix
     * @param numOfMatrixCols count of columns in matrix
     */
    Matrix(size_t numOfMatrixRows, size_t numOfMatrixCols) : rows(numOfMatrixRows), cols(numOfMatrixCols) {
        data.resize(this->rows);
        for (size_t i = 0; i < this->rows; i++) data[i].resize(this->cols);
    }

    /**
     * @brief Constructor
     * @param numOfMatrixRows count of rows in matrix
     * @param numOfMatrixCols count of columns in matrix
     * @param mat matrix
     */
    Matrix(size_t numOfMatrixRows, size_t numOfMatrixCols, const vector<int> &mat)
        : rows(numOfMatrixRows), cols(numOfMatrixCols) {
        int lastRow = -1;
        data.reserve(numOfMatrixRows);
        for (size_t i = 0; i < mat.size(); i++) {
            if ((i % numOfMatrixCols) == 0) {
                vector<int> v;
                v.reserve(numOfMatrixCols);
                data.push_back(v);
                lastRow++;
            }
            data[lastRow].push_back(mat[i]);
        }
    }

    /**
     * @brief Method returns the number of rows of the matrix
     * @return number of rows of the matrix
     */
    size_t getRowCount() const { return rows; }

    /**
     * @brief Method returns the number of columns of the matrix
     * @return number of columns of the matrix
     */
    size_t getColCount() const { return cols; }

    /**
     * @brief Method sets an item of the matrix
     * @param value new value
     * @param row index of row
     * @param col index of column
     */
    void set(int value, size_t row, size_t col) { data[row][col] = value; }

    /**
     * @brief Method sets an item of the matrix
     * @param value new value
     * @param i index
     */
    void set(int value, Index2D i) { set(value, i.row, i.col); }

    /**
     * @brief Get matrix item on given index
     * @param row index of row
     * @param col index of col
     * @return value on given index
     */
    int get(size_t row, size_t col) { return data[row][col]; }

    /**
     * @brief Get matrix item on given index
     * @param i index
     * @return value on given index
     */
    int get(Index2D i) { return get(i.row, i.col); }

    /**
     * @brief Method for printing out the multiplication result
     */
    void print() {
        cout << getRowCount() << ":" << getColCount() << endl;
        for (size_t row = 0; row < getRowCount(); row++) {
            for (size_t col = 0; col < getColCount(); col++) {
                cout << get(row,col);
                if ((col + 1) != getColCount()) cout << " ";
            }
            cout << endl;
        }
    }
};

/**
 * Class represents a single CPU in the mesh
 */
class CPU {
public:
    int cpuId, numOfCPUs;
    int rows, cols;
    int rowId, colId;
    bool firstRow, firstCol;
    bool lastRow, lastCol;
public:
    /**
     * @brief Constructor
     * @param cpuId CPU ID
     * @param numOfCPUs count of all CPUs
     * @param numOfMatrixRows count of rows in matrix
     * @param numOfMatrixCols count of columns in matrix
     */
    CPU(int cpuId, int numOfCPUs, int numOfMatrixRows, int numOfMatrixCols)
        : cpuId(cpuId), numOfCPUs(numOfCPUs), rows(numOfMatrixRows), cols(numOfMatrixCols) {
        rowId = cpuId / numOfMatrixCols;
        colId = cpuId % numOfMatrixCols;
        firstRow = rowId == 0;
        firstCol = colId == 0;
        lastRow = (rowId + 1) == numOfMatrixRows;
        lastCol = (colId + 1) == numOfMatrixCols;
    }

    /**
     * @brief Method returns the CPU ID of the left CPU
     * @return CPU ID
     */
    int getLeftCPUId() { return (firstCol) ? MAIN_CPU_ID : cpuId-1; }

    /**
     * @brief Method returns the CPU ID of the right CPU
     * @return CPU ID
     */
    int getRightCPUId() { return (lastCol) ? -1 : cpuId+1; }

    /**
     * @brief Method returns the CPU ID of the upper CPU
     * @return CPU ID
     */
    int getUpCPUId() { return (firstRow) ? MAIN_CPU_ID : cpuId-cols; }

    /**
     * @brief Method returns the CPU ID of the bottom CPU
     * @return CPU ID
     */
    int getDownCPUId() { return (lastRow) ? -1 : cpuId + cols; }

    /**
     * @brief Method returns the CPU ID of the first CPU in a row
     * @param row matrix row
     * @return CPU ID
     */
    int getIdFirstInRow(int row) { return (row >= rows) ? -1 : row * cols; }

    /**
     * @brief Method returns the CPU ID of the first CPU in a column
     * @param col matrix row
     * @return CPU ID
     */
    int getIdFirstInCol(int col) { return (col >= cols) ? -1 : col; }

    /**
     * @brief Method converts CPU ID to his index in matrix
     * @param id CPU ID
     * @param cols count of columns of matrix
     * @return index in matrix
     */
    static Index2D idToIndex2d(int id, int cols) {
        return Index2D((size_t) (id / cols), (size_t) (id % cols));
    }
};

// Function declarations
timespec timeDiff(timespec, timespec);
void mpiRecvInt(int*, int, int);
vector<int> readNumbers(fstream &);
Matrix readMatrix1(string);
Matrix readMatrix2(string);
void checkInputDimensions(Matrix &, Matrix &);
void checkProcessCount(int, int, int);
int processData(CPU &, Matrix &, Matrix &);
Matrix recvFinalData(CPU &, size_t, size_t);
